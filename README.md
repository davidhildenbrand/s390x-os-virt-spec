# s390x-os-virt-spec

Specifications for open-source virtualization on s390x (IBM z Systems), originating from the VIRTIO-based QEMU/KVM hypervisor. Also QEMU/TCG follows these specifications. This project aims to document, for example, hypercalls.

## Specifications

- [DIAGNOSE Code 0x500](diag500.md), used for implementing hypercalls.
