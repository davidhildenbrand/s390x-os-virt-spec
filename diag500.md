# DIAGNOSE Code 0x500

QEMU on s390x (IBM z Systems) uses the DIAGNOSE instruction to implement custom
hypercalls.

The [z/Architecture Principles of Operation](https://www.ibm.com/resources/publications/OutputPubsDetails?PubID=SA22-7832)
documents the DIAGNOSE instruction, excluding the actual instruction format or
any diagnostic function.
The [z/VM: CP Programming Services](https://www-01.ibm.com/servers/resourcelink/svc0302a.nsf/pages/zVMV7R2sc246272?OpenDocument)
document specifies one possible instruction format and lists IBM-supplied
DIAGNOSE codes.

QEMU uses the same instruction format as z/VM and reserved the **code 0x500**
for its purposes, defining subcodes that specify an actual hypercall.
The [Virtual I/O Device (VIRTIO) Standard](https://docs.oasis-open.org/virtio/virtio/) documents one
subcode.

This document specifies the behavior of the DIAGNOSE instruction with code
0x500, including all defined subcodes.

## DIAGNOSE Instruction Format

The DIAGNOSE instruction as implemented by QEMU uses the following format:

```
[  0x83 |  RxRy | B |      DDD ]
0       8      16  20         31

[ 0- 7] 0x83 is the opcode
[ 8-15] RxRy is unused for code 0x500
[16-19] B is the base register
[20-31] DDD is the displacement
```

### Specifying the Code

The code is calculated using the base register content, adding the displacement,
and using bit 48-63 of the result.
Note that specifying general register 0 as base register is recommended and
will result in the content of the register being ignored: consequently, only the
displacement specifies the resulting code.

## DIAGNOSE Code 0x500

DIAGNOSE code 0x500 was only ever defined and used in the z/Architecture mode.

General registers are 64 bit wide.
Any addresses used are 64 bit wide, independent of the addressing mode, unless
specified otherwise.

### Specifying the Subcode

General register 1 specifies the subcode.

### Input Parameter Passing

The input parameters used depend on the actual subcode.
In general, the first input parameter is passed in general register 2, the
second one in general register 3 and so on.

### Output Parameter Passing

The output parameters used depend on the actual subcode.
In general, the first value is returned in general register 2, the second one in
general register 3 and so on.

A subcode will not modify other registers than those general registers used for
returning values.
Subcodes not returning any values will not modify any register content.

### Resulting Condition Code

The condition code remains unchanged.

### Program Exceptions

- _Privileged operation_: Execution from problem state will result in a
  privileged operation exception.
- _Specification_: Unimplemented subcodes will result in a specification
  exception.

## Defined Subcodes

Subcode 0x0, 0x1 and 0x2 were used for implementing the legacy VirtIO device
model, that was never officially documented.
Subcode 0x3 is used for implementing virtio-ccw, officially documented in the
[VIRTIO Standard](https://docs.oasis-open.org/virtio/virtio/).

### 0x0 - Reserved (legacy)

Subcode 0x0 was used to notify the queue of VirtIO device that relied on the
legacy VirtIO transport. Even newer QEMU that did not support legacy VirtIO
transport implemented this hypercall.

The legacy VirtIO transport used virtqueues located in a memory area beyond
the maximum storage address exposed via SCLP. General register 2 specifies the
absolute address of the start of the queue to be notified ("vring") in the
physical address space. Notifications targeting memory locations smaller than
the maximum storage address (and therefore not targeting a valid queue) were
used to implement "early printk" and should be ignored by setting general
register 2 to 0.

### 0x1 - Reserved (legacy)

Subcode 0x1 was used for resetting VirtIO devices that relied on the legacy
VirtIO transport. QEMU no longer implements this subcode.

### 0x2 - Reserved (legacy)

Subcode 0x2 was used for updating the device status of VirtIO devices that
relied on the legacy VirtIO transport. QEMU no longer implements this subcode.

### 0x3 - VirtIO CCW Notification

Subcode 0x3 notifies a queue of a VirtIO CCW device about updates.
It may return a host cookie on success, to be used to speed up successive
notifications.

This subcode is also documented in the
[VIRTIO Standard](https://docs.oasis-open.org/virtio/virtio/).

#### Input Parameters

| General Register | Description |
| :------: | ------ |
| 2 | The subchannel ID. Only bit 32-63 are used. |
| 3 | The virtqueue number (queue index). |
| 4 | An optional host cookie, used to speed up successive notifications. |

If the host cookie is wrong or contains garbage, it is ignored.
The host cookie may change on successive calls.

#### Output Parameters

General register 2 contains the return code of the operation.

| Value | Description |
| ------: | ------ |
| &gt;= 0 | Success. The value may represent a host cookie and should be used for successive notifications of the same VirtIO CCW queue. |
| -22 | The subchannel identification is invalid, the subchannel cannot be found or is invisible, or the virtqueue number is too big. |
